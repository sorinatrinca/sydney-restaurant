# Sydney Restaurant Project

The project was created in order to familiarize myself with the Bootstrap framework and the SCSS syntax, using SASS as a preprocessor.

## Features

- Responsive pages
- Sections: header, about-us, menu, reviews, gallery, news & events, contact, footer


## Installation

Install the dependencies and start the server.

```sh
npm install
```

## Building for source

```sh
sass --watch scss/main.scss css/main.css
```

## Preview
![Preview Image](https://bitbucket.org/sorinatrinca/sydney-restaurant/raw/master/img/Preview.png)
